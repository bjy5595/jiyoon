
# 각 인물별 다차원(128차원) 얼굴 벡터를 생성

import sys
sys.path.append('../')

import glob
from imutils import paths
import face_recognition
import argparse
import pickle #파이썬 객체를 있는 그대로 저장하기 위한 모듈
import cv2
import os
import json
import numpy
import common_util


#NEXTLAB_MEMBERS = ['이창근', '전은비', '조은비', '이지웅', '이세용', '길근오', '박재현', '김성민', '윤영주', '김기범', '안형선', '김상윤', '정용진', '탁희준', '강희기']
NEXTLAB_MEMBERS = ['전은비', '조은비', '이세용', '길근오', '박재현', '김성민', '김기범', '백지윤', '김가영','박영수']
BASE_FOLDER_PATH = "data/nextlab_face"


#multi_json_data=open("data/@multi/face_info.json", encoding='utf-8').read()
#multi_face_dict = common_util.load_json(os.path.join(BASE_FOLDER_PATH, "@multi", "face_info.json"))
#print(multi_face_dict)


embedding_dict = {}

# dataset = 'datasets'
# encodings= 'encodings.pickle'
detection_method = 'cnn'


# 한글 경로가 포함된 경우에도 불러올 수 있음
def get_opencv_image(path):
    stream = open(path, "rb")
    bytes = bytearray(stream.read())
    numpyarray = numpy.asarray(bytes, dtype=numpy.uint8)
    bgr_image = cv2.imdecode(numpyarray, cv2.IMREAD_UNCHANGED)

    return resize(bgr_image)

# width, heigth 중 큰 값을 맞추어서 resize함
def resize(image, max_size = 1920):
    height, width = image.shape[:2]
    if width <= 1920 and height <= 1920:
        return image
    
    if width > height:
        width2 = max_size
        height2 = int(height * max_size / width)
    else:
        height2 = max_size
        width2 = int(width * max_size / height)

    return cv2.resize(image, (width2, height2), interpolation=cv2.INTER_CUBIC)

def get_face_embeddings(image_path):
    print(image_path)
    # image = cv2.imread(image_path)
    image = get_opencv_image(image_path)
    # print(image)
    rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

    # 불러들인 이미지에서 인식한 얼굴의 bounding box x,y 좌표를 검출
    boxes = face_recognition.face_locations(rgb, model = detection_method)

    # x1 값으로 오름차순 정렬
    boxes.sort(key=lambda box: box[3])

    # 다차원 얼굴 벡터 계산
    encodings = face_recognition.face_encodings(rgb, boxes)

    return (boxes, encodings)


# local_path = r"E:\GIT\face_tutorial\nextlab\data\NEW\output\IMG_20190824_1838399.jpg"
# boxes = get_face_embeddings(local_path)
# print(len(boxes), boxes)
# exit()


######################
# 1. 개인별 이미지 처리
######################

# for fn in glob.glob(FOLDER_MEMBER_EXT):
for member in NEXTLAB_MEMBERS:
    member_folder = os.path.join(BASE_FOLDER_PATH, member)
    if os.path.exists(member_folder):
        image_paths = list(paths.list_images(member_folder))
        for image_path in image_paths:
            print(image_path)
            boxes, embeddings = get_face_embeddings(image_path)

            member_embeddings = embedding_dict.get(member, [])
            member_embeddings.extend(embeddings)

            embedding_dict[member] = member_embeddings

'''
######################
# 2. 단체 이미지 처리
######################

folder_path = os.path.join(BASE_FOLDER_PATH, "@multi")
# folder_path = r"D:\DATA\face\조은비"

# 이미지 데이터가 있는 경로를 imagePaths 변수에 저장
print("[INFO] quantifying faces...")
imagePaths = list(paths.list_images(folder_path))
print(imagePaths)

# known encodings과 known names 변수 초기화
# knownEncodings = []
# knownNames = []


# def sort_function()

# for loop을 실행
for i, imagePath in enumerate(imagePaths):
    # 이미지 경로에서 사람의 이름을 추출
    print("[INFO] processing image {}/{}".format(i + 1, len(imagePaths)))
    # name = imagePath.split(os.path.sep)[-2]
    if "BAK" in imagePath:
        continue
    
    file_name = os.path.basename(imagePath)

    boxes, encodings = get_face_embeddings(imagePath)
    
    # loop over the encodings
    print(file_name, "얼굴수:", len(encodings))
    for i, encoding in enumerate(encodings):
        # add each encoding + name to our set of known names and
        # encodings
        print(file_name, i)
        member = multi_face_dict.get(file_name)[i]
        if member in NEXTLAB_MEMBERS:
            # print(member, encoding)
            
            member_embeddings = embedding_dict.get(member, [])
            member_embeddings.append(encoding)

            embedding_dict[member] = member_embeddings
'''
# print(embedding_dict)
# print(len(embedding_dict))

for member, member_embeddings in embedding_dict.items():
    print(member, len(member_embeddings))
# 다차원 얼굴 백터 저장
# print("[INFO] serializing encodings...")
# data = {"encodings": knownEncodings, "names": knownNames}
f = open("face_embedding.pkl", "wb")
f.write(pickle.dumps(embedding_dict))
f.close()
